#!/bin/bash
############
## MIT License
## Copyright (c) 2019-2022 Sebastien Delcroix (Seb) dev/at/delX/dot/io
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
############
file=$1
auth_srv1=$2
auth_srv2=$3
record_type=$4

resolve()
{
    local record_type="$1"
    local request="$2"
    local auth_srv="$3"

    dig +short @$auth_srv $record_type $request | sort | tr "\n" " "
}

for request in $(cat $file)
do
   req1=$(resolve $record_type $request $auth_srv1)
   req2=$(resolve $record_type $request $auth_srv2)
   echo -n "$request => '$req1'/'$req2' => "
   if [ "$req1" = "$req2" ]
   then
      echo "OK"
   else
      echo "FAILED"
   fi
done
