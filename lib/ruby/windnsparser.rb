require 'dnstools'
############
## MIT License
## Copyright (c) 2019-2022 Sebastien Delcroix (Seb) dev/at/delX/dot/io
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
############

class WinDNSParser

    DEBUG=false

    RE_COMMENT = Regexp.new(/^\s*;.*/)
    RE_STARTML = Regexp.new(/\s\(\s?/)
    RE_STOPML   = Regexp.new(/\s\)\s*;?.*$/)
    RE_VOIDLINE = Regexp.new(/^\s*$/)

    RE_REC_A = { :re => Regexp.new(/^([^\s]+)\s+(([0-9]*)\s*)A\s+([^\s]+)\s*$/), :type => "A" }
    RE_REC_AAAA = { :re => Regexp.new(/^([^\s]+)\s+(([0-9]*)\s*)AAAA\s+([^\s]+)\s*$/), :type => "AAAA" }
    RE_REC_MX = { :re => Regexp.new(/^([^\s]+)\s+(([0-9]*)\s*)MX\s+([0-9]+)\s+([^\s]+)\s*$/), :type => "MX" }
    RE_REC_NS = { :re => Regexp.new(/^([^\s]+)\s+(([0-9]*)\s*)NS\s+([^\s]+)\s*$/), :type => "NS" }
    RE_REC_PTR = { :re => Regexp.new(/^([^\s]+)\s+(([0-9]*)\s*)PTR\s+([^\s]+)\s*$/), :type => "PTR" }
    RE_REC_SOA = { :re => Regexp.new(/^([^\s]+)\s+(([0-9]*)\s*)IN\s+SOA\s+([^\s]+)\s+([^\s]+)\s+\(\s+(\d+)\s+[^\d]+(\d+)[^\d]+(\d+)[^\d]+(\d+)[^\d]+(\d+)\s+\).*$/), :type => "SOA" }
    RE_REC_TXT = { :re => Regexp.new(/^([^\s]+)\s+(([0-9]*)\s*)TXT\s+\(\s+([^\)]+)\).*$/), :type => "TXT" }
    RE_REC_CNAME = { :re => Regexp.new(/^([^\s]+)\s+(([0-9]*)\s*)CNAME\s+([^\s]+)\s*$/), :type => "CNAME" }
    RE_REC_SRV = { :re => Regexp.new(/^([^\s]+)\s+(([0-9]*)\s*)SRV\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)\s+([^\s]+)\s*$/), :type => "SRV" }


    RE_LIST = [ RE_REC_A, RE_REC_AAAA, RE_REC_MX, RE_REC_NS, RE_REC_SOA, RE_REC_TXT, RE_REC_CNAME, RE_REC_SRV , RE_REC_PTR]

    attr_reader :data

    def initialize(filepath,domain)
        @dns_file_path = filepath
        @domain = domain
        @data = Hash.new
        @default_ttl = ""
    end

    def parse()

        ml = false
        raw_data = ""
        previous_entry=""
        File.read(@dns_file_path).each_line do |line|
            
            line.chomp!

            printDebug line

            if line.match(RE_COMMENT)
                printDebug "COMMENT"
                next
            end

            if line.match(RE_VOIDLINE)
                printDebug "VOID"
                next
            end

            [RE_STOPML,RE_STARTML].each do |re|

                if line.match(re)
                    printDebug re.to_s
                end
            end

            if ! ml
                printDebug "NO ML"
                if line.match(RE_STARTML) && ! line.match(RE_STOPML)
                    raw_data << line
                    ml=true
                    printDebug "ML ON"
                    previous_entry=""
                    next
                else
                    raw_data << line
                end
            else
                printDebug "ML ENAB"
                if line.match(RE_STOPML)
                    raw_data << line
                    printDebug "ML OFF"
                else
                    raw_data << line
                    next
                end
            end


            if raw_data.match(/^\s/) && previous_entry != ""
                raw_data = previous_entry+raw_data
            end

            re = raw_data.match(/^([^\s]+)\s.*/)
            if re
                previous_entry = re[1]
            end

            formatted_data = nil
            entry = ""
            #puts "Candidate: #{raw_data}"

            RE_LIST.each do |recheck|
                re = raw_data.match(recheck[:re])
                if re
                    entry,formatted_data = extract_data(raw_data,recheck[:type],re)
                    @data[entry] ||= Hash.new
                    @data[entry][recheck[:type]] ||= Array.new
                    @data[entry][recheck[:type]] << formatted_data
                    break
                end
            end

            if ! formatted_data
                STDERR.write("#{@domain}: No match => #{line}\n")
            # else
            #     STDERR.write("#{@domain}: Match OK => #{line}\n")
            end

            raw_data = ""
            ml = false

        end
    end
    private

    def extract_data(data,type,re)

        result = Hash.new
        printDebug "#{type} => #{data}"
        request = normalizeEntry(re[1])

        case type
        when "A"   
            ttl = (re[3] == "") ? @default_ttl : re[3]
            result = {:ttl => ttl, :address => re[4] , :request => request}
        when "AAAA"   
            ttl = (re[3] == "") ? @default_ttl : re[3]
            result = {:ttl => ttl, :address => re[4] , :request => request}

        when "SOA"
            @default_ttl = re[10]
            ttl = (re[3] == "") ? @default_ttl : re[3]
            result =    {   :mname => normalizeName(re[4]),
                            :rname => normalizeName(re[5]),
                            :serial => re[6],
                            :refresh => re[7],
                            :retry => re[8],
                            :expire => re[9],
                            :minimum => re[10],
                            :request => request,
                            :ttl => ttl }
        
        when "TXT" 
            ttl = (re[3] == "") ? @default_ttl : re[3]
            values = re[4].scan(/\"(.+?)\"/).flatten.map { |value| unescapeStr(value)}
            result = {:ttl => ttl, :strings => values, :request => request}
        
        
        when "CNAME" 
            ttl = (re[3] == "") ? @default_ttl : re[3]
            result = {:ttl => ttl, :name => normalizeName(re[4]),:request => request}            

        when "NS" 
            ttl = (re[3] == "") ? @default_ttl : re[3]
            result = {:ttl => ttl, :name => normalizeName(re[4]), :request => request}

        when "PTR" 
            ttl = (re[3] == "") ? @default_ttl : re[3]
            result = {:ttl => ttl, :name => normalizeName(re[4]), :request => request}

        when "SRV" 
            ttl = (re[3] == "") ? @default_ttl : re[3]
            result =    {   :priority => re[4],
                            :weight => re[5],
                            :port => re[6],
                            :target => normalizeName(re[7]),
                            :request => request,
                            :ttl => ttl
                        }

        when "MX"
            ttl = (re[3] == "") ? @default_ttl : re[3]
            result = {:ttl => ttl, :preference => re[4], :exchange => normalizeName(re[5]),:request => request}            
                
        end

        uniq_id = DNSTools::uniqID(result,type)
        result[:uniq_id]= uniq_id

        return request,result
    end
   

    def unescapeStr(str)
        return str.gsub(/\\([0-7]{3})/i){ octalToChr($1) }
    end

    def octalToChr(octal_str)
        return octal_str.to_i(8).chr
    end

    def normalizeName(name)
        return name.gsub(/\.$/,"")
    end

    def normalizeEntry(entry)

        if entry == "@"
            return @domain
        end

        if ! entry.match(/\.$/)
            return entry+"."+@domain
        end

        return entry
    end

    def printDebug(msg)
        STDERR.write(msg+"\n") if DEBUG
    end
end