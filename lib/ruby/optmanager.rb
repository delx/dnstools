#optmanager
############
## MIT License
## Copyright (c) 2019-2022 Sebastien Delcroix (Seb) dev/at/delX/dot/io
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
############
require 'getoptlong'


 
#{
#  :name => "program ...",
#  :version => "1.2",  
#  :opts => [
# { 
#    :opt => { :long => '--help', :short => '-h', :requirement => GetoptLong::NO_ARGUMENT },
#    :help => "blah blah",
#    :default => ????
#}    
# ]
#  :examples => [ "ex1" , "ex2"]
#}

#GetoptLong::OPTIONAL_ARGUMENT

class OptManager


    attr_reader :param

    def initialize(opts)
        @param = Hash.new
        @opts = opts
        @getopt_param = Array.new()

        @opts[:opts].each do |opt|
            @getopt_param << [ opt[:opt][:long], opt[:opt][:short], opt[:opt][:requirement]]
        end

    end

    def parse()

        opts = GetoptLong.new(*@getopt_param)

        opts.each do |foundopt, arg|
            @opts[:opts].each do |opt|
                if foundopt == opt[:opt][:long]
                    case  opt[:opt][:requirement]
                    when GetoptLong::NO_ARGUMENT
                        @param[foundopt] = nil
                    when GetoptLong::REQUIRED_ARGUMENT
                        @param[foundopt] = arg
                    when GetoptLong::OPTIONAL_ARGUMENT
                        if arg == nil and opt.has_key?(:default)
                            @param[foundopt] = opt[:default]
                        elsif arg != nil
                            @param[foundopt] = arg
                        else
                            @param[foundopt] = nil
                        end
                    end
                end
            end
        end
    end



    def usage(msg="")

        if msg != ""
            msg << "\n"
        end

        puts <<-EOH
#{@opts[:name]} V#{@opts[:version]}
#{@opts[:name]} [OPTION] 
#{msg}
Usage:
EOH

        @opts[:opts].each do |opt|
            arg = ""
            case opt[:opt][:requirement]
            when GetoptLong::NO_ARGUMENT
                arg = ""
            when GetoptLong::REQUIRED_ARGUMENT
                arg = " param"
            when GetoptLong::OPTIONAL_ARGUMENT
                arg = "param"
                if opt.has_key?(:default)
                    arg << ", default = #{opt[:default]}" if opt[:default] != nil
                end
                arg = " [#{arg}]"
            end
            puts <<-EOO
  - #{opt[:opt][:short]}, #{opt[:opt][:long]}#{arg}: #{opt[:help]}
EOO
        end

        if @opts.has_key?(:examples)
            puts "\nExamples:"
            @opts[:examples].each do |example|
                puts "  - #{@opts[:name]} "+example.gsub("\n","\n      ")
            end
        end

    end

end