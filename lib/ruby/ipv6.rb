#encoding: UTF-8
############
## MIT License
## Copyright (c) 2019-2022 Sebastien Delcroix (Seb) dev/at/delX/dot/io
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
############

class IPV6Error < RuntimeError
    NOT_AN_IPV6="not an IPV6"
end

class IPV6

    attr_accessor :ipv6
    
    def initialize(ipv6)
    
        unless self.class.is_ipv6?(ipv6)
            raise(IPV6Error,IPV6Error::NOT_AN_IPV6)
        end
        
        @ipv6 = ipv6
    end

    def self.is_ipv6?(ipv6)
    
        unless ipv6 =~ /^[0-9a-f]{0,4}:[0-9a-f]{0,4}:[0-9a-f]{0,4}:[0-9a-f]{0,4}:[0-9a-f]{0,4}:[0-9a-f]{0,4}:[0-9a-f]{0,4}:[0-9a-f]{0,4}$/
            return false
        end
    
        if ipv6 =~ /.*::.*::.*/
            return false
        end
    
        ipv6.split(':').each do |word|
            if word.to_i(16) > 65535
                return false
            end
        end
        
        return true
    end
    
    def to_s
        @ipv6
    end

end
