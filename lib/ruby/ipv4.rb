#encoding: UTF-8
############
## MIT License
## Copyright (c) 2019-2022 Sebastien Delcroix (Seb) dev/at/delX/dot/io
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
############
class IPV4Error < RuntimeError
    NOT_AN_IPV4="not an IPV4"
end
    
class IPV4

    attr_accessor :ipv4
    
    def initialize(ipv4)
    
        unless self.class.is_ipv4?(ipv4)
            raise(IPV4Error,IPV4Error::NOT_AN_IPV4)
        end
        
        @ipv4 = ipv4
    end
    
    def self.is_ipv4?(ipv4)
    
        unless ipv4 =~ /^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/
            return false
        end
    
        ipv4.split('.').each do |byte|
            if byte.to_i > 255
                return false
            end
        end
        
        return true
    end
    
    def to_s
        @ipv4
    end
    
end
