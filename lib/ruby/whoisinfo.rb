require "whois-parser" # gem install whois-parser
require 'fileutils'
require 'netinfo'
############
## MIT License
## Copyright (c) 2019-2022 Sebastien Delcroix (Seb) dev/at/delX/dot/io
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
############
# record = raw data
# nameservers
#   [
#      nameserver
#       {
#         - :name
#         - :ipv4
#         - :ipv6
#       }
#   ]
# contacts
#   [
#       contact
#      {
#         - id=nil
#         - type
#         - name
#         - organization
#         - address
#         - city
#         - zip
#         - state
#         - country
#         - country_code
#         - phone
#         - fax=
#         - email
#         - url
#         - created_on
#         - updated_on
#      } 
#   ] 
# parser.presence.parsers[0]
# 



class WhoisInfoError < RuntimeError

    BAD_OBJECT="neither an IP or a domain"

end

class WhoisInfo

    attr_reader :data
    attr_reader :status
    
    def initialize(search_obj,config=nil)
    
        if config != nil
            @cache = config
        else
            @cache = {:path=>nil,:ttl=>nil,:enaled=>false}
        end
        
        @search_obj = search_obj 
        @data = {}
        @status = { :from => nil, :cached => nil}
    end
    
    
    
    def getInfo
        if cacheEnabled? and cacheHasObject?
            unless readFromCache
                gatherInfo
            end
        else
            gatherInfo
        end
    end

    private
    
    
    def gatherInfo
    
    
        @data = {}
    
        if DomainName::is_domain?(@search_obj) or NetInfo::is_ip?(@search_obj)
            #~ puts "gathering data #{@search_obj} online"
            parsed = Whois.whois(@search_obj).parse

             @data[:record] =
            @data[:date] = Time.now.to_i
            @status[:from] = :online
        else
            raise(WhoisInfoError,WhoisInfoError::BAD_OBJECT)
        end
        
        if cacheEnabled?
            saveToCache
        end
    
    end
    
    def readFromCache
    
        @data = Marshal.load(File.read(cachePath))
        if @data[:date] + @cache[:ttl] < Time.now.to_i
            @data = nil
            return false
        end
        @status[:from] = :cache
        @status[:cached] = :true

        return true
    end
    
    def cacheHasObject?
    
        if File.exists?(cachePath)
            return true
        end
        
        return false
    
    end
    
    def cachePath
        return @cache[:path]+"/"+@search_obj
    end
    
    def cacheEnabled?
    
        if @cache.has_key?(:enabled) and @cache[:enabled] == true
            return true
        end
        
        return false
    end


    def saveToCache
        unless Dir.exists?(@cache[:path])
            FileUtils.mkdir_p(@cache[:path])
        end
    
        fd = File.open(cachePath,"w")
        fd.write(Marshal.dump(@data))
        fd.close
        @status[:cached] = :true
    end

end
