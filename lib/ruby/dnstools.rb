
require 'digest'
############
## MIT License
## Copyright (c) 2019-2022 Sebastien Delcroix (Seb) dev/at/delX/dot/io
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
############

class DNSTools

    ID_RECORDS = {
                    "A" => [:address,:ttl,:request],
                    "CNAME" => [:name,:ttl,:request],
                    "PTR" => [:name,:ttl,:request],
                    "NS" => [:name,:ttl,:request],
                    "TXT" => [:strings,:ttl,:request],
                    "SOA" => [:mname,:rname,:serial,:refresh,:retry,:expire,:minimum,:ttl,:request],
                    "SRV" => [:priority,:weight,:port,:target,:ttl,:request],
                    "MX" => [:preference,:exchange,:ttl,:request]
                }

    def self.uniqID(record,resource_type)
        str=""

        if ! ID_RECORDS.has_key?(resource_type)
            return nil
        end
        ID_RECORDS[resource_type].each do |type_value|
            str << record[type_value].to_s
        end

        return Digest::SHA512.hexdigest(str)
    end

    def self.compareRecords(record1,record2)

        if record1.length != record2.length
            return false
        end

        record1.each do |record|
            if ! record.has_key?(:uniq_id)
                return false
            end
            found=false
            record2.each do |s_record|
                if record[:uniq_id] == s_record[:uniq_id]
                    found = true
                    break
                end
            end 

            # if ! found
            #     return false
            # end
            return found
            #TODO miss a return somewhere should be: return found
        end
    end


    def self.compareNRecords(records)

        comb = Hash.new
        #p records
        pos_comb = Array.new
        records.length.times {|id| pos_comb << id} 
        pos_comb.permutation(2) {|perm| comb[perm.sort.join(',')] ||= perm }

        comb.values do |key,value| 
            if ! self.compareRecords(records[value[0]],records[value[1]])
              return false
            end
        end

        return true
    end
end