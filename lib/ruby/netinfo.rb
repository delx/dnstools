#encoding: UTF-8
############
## MIT License
## Copyright (c) 2019-2022 Sebastien Delcroix (Seb) dev/at/delX/dot/io
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
############
require 'dnsinfo'
require 'domainname'
require 'ipv4'
require 'ipv6'
require 'whoisinfo'
require 'search'
require 'datamining'



class NetInfoError < RuntimeError
    NOT_AN_IP="not an IP(V4 or V6)"
end

module NetInfo

    class IPV4 < IPV4
    end
    
    class IPV6 < IPV6
    end

    class DomainName < DomainName
    end
    
    def self.is_ip?(ip)
        if IPV4.is_ipv4?(ip)
            return true
        end
        
        if IPV6.is_ipv6?(ip)
            return true
        end
        
        return false
    end

end
