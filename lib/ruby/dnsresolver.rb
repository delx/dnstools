
require 'resolv'
############
## MIT License
## Copyright (c) 2019-2022 Sebastien Delcroix (Seb) dev/at/delX/dot/io
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
############
class DNSResolver
    
    @@RESOURCES_LIST = [ "ANY", "A", "MX", "SOA", "NS", "TXT", "SRV", "PTR" , "CNAME"]

    def self.to_csv(records,fields_list)

        result = ""
        records.each do |record|
            csv = Array.new
            fields_list.each do |field|
                csv << record[field]
            end
            result << csv.join(",")+"\n"
        end

        return result
    end

    def self.csv_header(fields_list)
        csv = Array.new

        fields_list.each do |field|
            csv << field.to_s
        end

        return csv.join(",")+"\n"
    end

    def initialize(resolver)
        @resolver = resolver
    end


    def resolve(request,resource_type)

        result = nil
        case resource_type
        when "A"
            result = resolve_A(request)
        when "AAAA"
            result = resolve_AAAA(request)
        when "CNAME"
            result = resolve_CNAME(request)
        when "NS"
            result = resolve_NS(request)
        when "SOA"
            result = resolve_SOA(request)
        when "TXT"
            result = resolve_TXT(request)
        when "SRV"
            result = resolve_SRV(request)
        when "MX"
            result = resolve_MX(request)
        when "PTR"
            result = resolve_PTR(request)
        else
          return nil
        end

        return result
    end

    private


    def resolve_TXT(request)

        result = Array.new

        Resolv::DNS.open(:nameserver => [@resolver],:ndots => 1) do |dns|
            ress = dns.getresources request, Resolv::DNS::Resource::IN::TXT
            #p ress
            ress.each do |records|
                #uniq_id=Digest::SHA512.hexdigest(records.strings.to_s+records.ttl.to_s)
                a_record = { "strings": records.strings.to_s, 
                            "ttl":records.ttl.to_s, 
                            "record_type":"TXT",
                            "request":request,
                            "resolver": @resolver 
                        }
                uniq_id = DNSTools::uniqID(a_record,"TXT")
                a_record[:uniq_id] = uniq_id
                result << a_record
            end
        end

        return result
    end


    def resolve_A(request)

        result = Array.new

        Resolv::DNS.open(:nameserver => [@resolver],:ndots => 1) do |dns|
            ress = dns.getresources request, Resolv::DNS::Resource::IN::A

            ress.each do |records|
                a_record = { "address": records.address.to_s, 
                            "ttl":records.ttl.to_s, 
                            "record_type":"A",
                            "request":request,
                            "resolver": @resolver
                        }
                uniq_id = DNSTools::uniqID(a_record,"A")
                a_record[:uniq_id] = uniq_id
                result << a_record

                
            end
        end

        return result
    end

    def resolve_AAAA(request)

        result = Array.new

        Resolv::DNS.open(:nameserver => [@resolver],:ndots => 1) do |dns|
            ress = dns.getresources request, Resolv::DNS::Resource::IN::AAAA

            ress.each do |records|
                a_record = { "address": records.address.to_s, 
                            "ttl":records.ttl.to_s, 
                            "record_type":"A",
                            "request":request,
                            "resolver": @resolver
                        }
                uniq_id = DNSTools::uniqID(a_record,"AAAA")
                a_record[:uniq_id] = uniq_id
                result << a_record

                
            end
        end

        return result
    end

    def resolve_PTR(request)

        result = Array.new

        Resolv::DNS.open(:nameserver => [@resolver],:ndots => 1) do |dns|
            ress = dns.getresources request, Resolv::DNS::Resource::IN::PTR
            ress.each do |records|
                a_record = { "name": records.name.to_s, 
                            "ttl":records.ttl.to_s, 
                            "record_type":"PTR",
                            "request":request,
                            "resolver": @resolver
                        }
                uniq_id = DNSTools::uniqID(a_record,"PTR")
                a_record[:uniq_id] = uniq_id
                result << a_record
            end
        end

        return result
    end


    def resolve_CNAME(request)

        result = Array.new

        Resolv::DNS.open(:nameserver => [@resolver],:ndots => 1) do |dns|
            ress = dns.getresources request, Resolv::DNS::Resource::IN::CNAME

            ress.each do |records|
                #uniq_id=Digest::SHA512.hexdigest(records.name.to_s+records.ttl.to_s)
                a_record = { "name": records.name.to_s, 
                            "ttl":records.ttl.to_s,
                            "record_type":"CNAME",
                            "request":request,
                            "resolver": @resolver
                        }
                uniq_id = DNSTools::uniqID(a_record,"CNAME")
                a_record[:uniq_id] = uniq_id
                result << a_record
            end
        end

        return result
    end

    def resolve_NS(request)

        result = Array.new

        Resolv::DNS.open(:nameserver => [@resolver],:ndots => 1) do |dns|
            ress = dns.getresources request, Resolv::DNS::Resource::IN::NS
            ress.each do |records|
                #uniq_id=Digest::SHA512.hexdigest(records.name.to_s+records.ttl.to_s)
                a_record = { "name": records.name.to_s, 
                            "ttl":records.ttl.to_s,
                            "record_type":"NS",
                            "request":request,
                            "resolver": @resolver
                        }
                uniq_id = DNSTools::uniqID(a_record,"NS")
                a_record[:uniq_id] = uniq_id
                result << a_record
            end
        end

        return result
    end

    def resolve_MX(request)

        result = Array.new

        Resolv::DNS.open(:nameserver => [@resolver],:ndots => 1) do |dns|
            ress = dns.getresources request, Resolv::DNS::Resource::IN::MX
            ress.each do |records|
            #uniq_id=Digest::SHA512.hexdigest(records.preference.to_s+records.exchange.to_s+records.ttl.to_s)
            a_record = { "preference": records.preference.to_s, 
                        "exchange": records.exchange.to_s,
                        "ttl":records.ttl.to_s,
                        "record_type":"MX",
                        "request":request,
                        "resolver": @resolver
                    }
            uniq_id = DNSTools::uniqID(a_record,"MX")
            a_record[:uniq_id] = uniq_id
            result << a_record
            end
        end

        return result
    end

    def resolve_SOA(request)

        result = Array.new

        Resolv::DNS.open(:nameserver => [@resolver],:ndots => 1) do |dns|
            ress = dns.getresources request, Resolv::DNS::Resource::IN::SOA
            ress.each do |records|

                #uniq_id=Digest::SHA512.hexdigest(records.mname.to_s+records.rname.to_s+records.serial.to_s+records.refresh.to_s+records.retry.to_s+records.expire.to_s+records.minimum.to_s+records.ttl.to_s)

                a_record = { "mname": records.mname.to_s,
                            "rname": records.rname.to_s,
                            "serial": records.serial.to_s,
                            "refresh": records.refresh.to_s,
                            "retry": records.retry.to_s,
                            "expire": records.expire.to_s,
                            "minimum": records.minimum.to_s,
                            "ttl":records.ttl.to_s,
                            "record_type":"SOA",
                            "request":request,
                            "resolver": @resolver
                        }
                uniq_id = DNSTools::uniqID(a_record,"SOA")
                a_record[:uniq_id] = uniq_id
                result << a_record
            end
    
        end

        return result
    end

    def resolve_SRV(request)

        result = Array.new

        Resolv::DNS.open(:nameserver => [@resolver],:ndots => 1) do |dns|
            ress = dns.getresources request, Resolv::DNS::Resource::IN::SRV

            ress.each do |records|

            #uniq_id=Digest::SHA512.hexdigest(records.priority.to_s+records.weight.to_s+records.port.to_s+records.target.to_s+records.ttl.to_s)

            a_record = { "priority": records.priority.to_s,
                        "weight": records.weight.to_s,
                        "port": records.port.to_s,
                        "target": records.target.to_s,
                        "ttl":records.ttl.to_s,
                        "record_type":"SRV",
                        "request":request,
                        "resolver": @resolver
                    }

            uniq_id = DNSTools::uniqID(a_record,"SRV")
            a_record[:uniq_id] = uniq_id
            result << a_record
            end
    
        end

        return result
    end


end

