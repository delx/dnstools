
# Implementation

| Records  | Resolver  | Parser |
| :------- |:---------:| :-----:|
| A        |     X     |    X   |
| CNAME    |     X     |    X   |
| NS       |     X     |    X   |
| TXT      |     X     |    X   |
| SOA      |     X     |    X   |
| SRV      |     X     |    X   |
| MX       |     X     |    X   |
| PTR      |           |        |
| AAAA     |           |        |

